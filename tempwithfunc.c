#include<stdio.h>
 

float input()
{
 float x;
 printf("Enter temperature in Fahrenheit:\n");
 scanf("%f",&x);
 return x;
}

float conversion(float f)
{
 float c;
 c=(f-32)*0.55;
 return c;
}

void output(float cel)
{
printf("Celsius=%f",cel);
}
int main()
{
    float celsius,fahrenheit;
 
    fahrenheit=input();
    celsius=conversion(fahrenheit);
    output(celsius);
    return 0;
}